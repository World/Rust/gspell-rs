// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "GspellEntry")]
    pub struct Entry(Object<ffi::GspellEntry, ffi::GspellEntryClass>);

    match fn {
        type_ => || ffi::gspell_entry_get_type(),
    }
}

impl Entry {
    // rustdoc-stripper-ignore-next
    /// Creates a new builder-pattern struct instance to construct [`Entry`] objects.
    ///
    /// This method returns an instance of [`EntryBuilder`](crate::builders::EntryBuilder) which can be used to create [`Entry`] objects.
    pub fn builder() -> EntryBuilder {
        EntryBuilder::new()
    }

    #[doc(alias = "gspell_entry_basic_setup")]
    pub fn basic_setup(&self) {
        unsafe {
            ffi::gspell_entry_basic_setup(self.to_glib_none().0);
        }
    }

    #[doc(alias = "gspell_entry_get_entry")]
    #[doc(alias = "get_entry")]
    pub fn entry(&self) -> Option<gtk::Entry> {
        unsafe { from_glib_none(ffi::gspell_entry_get_entry(self.to_glib_none().0)) }
    }

    #[doc(alias = "gspell_entry_get_inline_spell_checking")]
    #[doc(alias = "get_inline_spell_checking")]
    pub fn is_inline_spell_checking(&self) -> bool {
        unsafe {
            from_glib(ffi::gspell_entry_get_inline_spell_checking(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gspell_entry_set_inline_spell_checking")]
    pub fn set_inline_spell_checking(&self, enable: bool) {
        unsafe {
            ffi::gspell_entry_set_inline_spell_checking(self.to_glib_none().0, enable.into_glib());
        }
    }

    #[doc(alias = "gspell_entry_get_from_gtk_entry")]
    #[doc(alias = "get_from_gtk_entry")]
    pub fn from_gtk_entry(gtk_entry: &impl IsA<gtk::Entry>) -> Option<Entry> {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_none(ffi::gspell_entry_get_from_gtk_entry(
                gtk_entry.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "inline-spell-checking")]
    pub fn connect_inline_spell_checking_notify<F: Fn(&Self) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn notify_inline_spell_checking_trampoline<F: Fn(&Entry) + 'static>(
            this: *mut ffi::GspellEntry,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::inline-spell-checking\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_inline_spell_checking_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

// rustdoc-stripper-ignore-next
/// A [builder-pattern] type to construct [`Entry`] objects.
///
/// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct EntryBuilder {
    builder: glib::object::ObjectBuilder<'static, Entry>,
}

impl EntryBuilder {
    fn new() -> Self {
        Self {
            builder: glib::object::Object::builder(),
        }
    }

    pub fn entry(self, entry: &impl IsA<gtk::Entry>) -> Self {
        Self {
            builder: self.builder.property("entry", entry.clone().upcast()),
        }
    }

    pub fn inline_spell_checking(self, inline_spell_checking: bool) -> Self {
        Self {
            builder: self
                .builder
                .property("inline-spell-checking", inline_spell_checking),
        }
    }

    // rustdoc-stripper-ignore-next
    /// Build the [`Entry`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> Entry {
        self.builder.build()
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Entry")
    }
}
