// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{Language, LanguageChooser};
use glib::{prelude::*, translate::*};
use std::fmt;

glib::wrapper! {
    #[doc(alias = "GspellLanguageChooserDialog")]
    pub struct LanguageChooserDialog(Object<ffi::GspellLanguageChooserDialog, ffi::GspellLanguageChooserDialogClass>) @extends gtk::Dialog, gtk::Window, gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable, LanguageChooser;

    match fn {
        type_ => || ffi::gspell_language_chooser_dialog_get_type(),
    }
}

impl LanguageChooserDialog {
    pub const NONE: Option<&'static LanguageChooserDialog> = None;

    #[doc(alias = "gspell_language_chooser_dialog_new")]
    pub fn new(
        parent: &impl IsA<gtk::Window>,
        current_language: Option<&Language>,
        flags: gtk::DialogFlags,
    ) -> LanguageChooserDialog {
        assert_initialized_main_thread!();
        unsafe {
            gtk::Widget::from_glib_none(ffi::gspell_language_chooser_dialog_new(
                parent.as_ref().to_glib_none().0,
                current_language.to_glib_none().0,
                flags.into_glib(),
            ))
            .unsafe_cast()
        }
    }
}

impl fmt::Display for LanguageChooserDialog {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("LanguageChooserDialog")
    }
}
