#![cfg_attr(docsrs, feature(doc_cfg))]
// Copyright 2013-2018, The Gtk-rs Project Developers.
// See the COPYRIGHT file at the top-level directory of this distribution.
// Licensed under the MIT license, see the LICENSE file or <http://opensource.org/licenses/MIT>

/// Asserts that this is the main thread and `gtk::init` has been called.
macro_rules! assert_initialized_main_thread {
    () => {
        if !::gtk::is_initialized_main_thread() {
            if ::gtk::is_initialized() {
                panic!("GTK may only be used from the main thread.");
            } else {
                panic!("GTK has not been initialized. Call `gtk::init` first.");
            }
        }
    };
}

/// No-op.
macro_rules! skip_assert_initialized {
    () => {};
}

pub use ffi;
pub use gdk;
pub use gdk_pixbuf;
pub use gio;
pub use glib;
pub use gtk;

#[allow(clippy::derived_hash_with_manual_eq)]
#[allow(clippy::new_ret_no_self)]
mod auto;
pub use auto::*;

pub mod prelude {
    pub use super::auto::traits::*;
}

pub mod builders {
    pub use super::auto::builders::*;
}
