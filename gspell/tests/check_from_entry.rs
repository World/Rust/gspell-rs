#[test]
fn check_from_entry() {
    assert!(gtk::init().is_ok());
    let entry = gtk::Entry::new();
    let gspell_entry = gspell::Entry::from_gtk_entry(&entry).unwrap();
    gspell_entry.basic_setup();
}
