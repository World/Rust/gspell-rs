# gspell-rs
This repository contains the rust bindings for gspell.

**Note**: gspell-rs is no longer actively maintained as well as other gtk3-rs crates.

Website: <https://world.pages.gitlab.gnome.org/Rust/gspell-rs>

## Documentation

- gspell: <https://world.pages.gitlab.gnome.org/Rust/gspell-rs/stable/latest/docs/gspell>
- gspell-sys: <https://world.pages.gitlab.gnome.org/Rust/gspell-rs/stable/latest/docs/gspell_sys>


## Using
Add this line to your Cargo file
```
[dependencies]
gtk = { git = "https://gitlab.gnome.org/World/Rust/gspell-rs" }
```

## Build
We use [gir](https://github.com/gtk-rs/gir) to generate rust gspell bindings. The bindings are split in two parts, sys and api.
```shell
git clone --recursive https://gitlab.gnome.org/World/Rust/gspell-rs.git && cd gspell-rs
./python3 generate.py
cargo build -p gspell
```

